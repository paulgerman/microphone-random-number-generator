'use strict';

// Create an instance
var wavesurfer = Object.create(WaveSurfer);

// Init & load
var elRandom, elRandomNum, elEntropy, elEntropyMax, elWeakMode, elStrongMode, elEntropyJS, elRandomJS;
document.addEventListener('DOMContentLoaded', function () {

    elRandom = document.getElementById("random");
    elRandomNum = document.getElementById("random_num");
    elEntropy = document.getElementById("entropy");
    elEntropyMax = document.getElementById("entropy_max");
    elWeakMode = document.getElementById("weak_mode");
    elStrongMode = document.getElementById("strong_mode");
    elEntropyJS = document.getElementById("entropyJS");
    elRandomJS = document.getElementById("randomJS");

    var options = {
        container     : '#waveform',
        waveColor     : 'black',
        interact      : true,
        cursorWidth   : 0
    };
    var micBtn = document.querySelector('#micBtn');

    // Init wavesurfer
    wavesurfer.init(options);

    // Init Microphone plugin
    var microphone = Object.create(WaveSurfer.Microphone);
    microphone.init({
        wavesurfer: wavesurfer
    });
    microphone.on('deviceReady', function() {
        console.info('Device ready!');
    });
    microphone.on('deviceError', function(code) {
        console.warn('Device error: ' + code);
    });

    // start/stop mic on button click
    micBtn.onclick = function() {
        if (microphone.active) {
            microphone.stop();
        } else {
            microphone.start();
        }
    };

    setInterval(function() {
    },1000);
});

var printed = false;


function processPeaks(arrPeaks)
{
    var sum = 0;
    for(var i in arrPeaks)
        if(arrPeaks[i] != 0)
            sum += Math.abs(arrPeaks[i]);
    var result = sum/arrPeaks.length;

    if(result > 0.000480204152568352593) {
        if(elWeakMode.checked)
        {
            while(result < 1){
                result *= 10;
            }
            result = Math.round(result);
        }
        else
        {
            var temp = Math.abs(sum);

            result = temp.toString();
            result = parseInt(result[result.length-2]);
        }

        if(!printed) {
            console.log(arrPeaks);
            printed = true;
        }
        var text = elRandom.value + result.toString();
        text = text.slice(-1 * parseInt(elRandomNum.value));
        elRandom.value = text;
        elEntropy.value = Shannon.entropy(text);
        elEntropyMax.value = Shannon.ideal(text);


        var textJS = elRandomJS.value + getRandomInt(0,9).toString();
        textJS = textJS.slice(-1 * parseInt(elRandomNum.value));
        elRandomJS.value = textJS;
        elEntropyJS.value = Shannon.entropy(textJS);
    }
}

class Shannon
{
    //dictionary of character frequencies
    static process(s, evaluator) {
        var h = {};
        var k;
        s.split('').forEach(function(c) {
            h[c] && h[c]++ || (h[c] = 1); });
        for (k in h)
            evaluator(k, h[k]);
        return h;
    }

    //Measure the entropy of string
    static entropy(s)
    {
        var sum = 0;
        var len = s.length;
        Shannon.process(s, function(k, f) {
            var p = f/len;
            sum -= p * Math.log(p) / Math.log(2);
        });
        return sum; //bits per symbol
    };

    static ideal(s) {
        return 3.321928094887362;
        /*
        var h = {};
        s.split('').forEach(function(c) {
            h[c] && h[c]++ || (h[c] = 1); });

        var prob = 1.0 / Object.keys(h).length;*/
        var prob = 1.0 / 10;
        return -1.0 * s.length * prob * Math.log(prob) / Math.log(2.0)
    }
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}